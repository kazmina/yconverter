package service;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import utils.*;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * Service implementation, that takes rate values from db
 * Created by tatiana.kazmina on 25.01.2018.
 */
@RestController
@RequestMapping("/yconverter")
public class ConverterServiceDBImpl implements ConverterService {
    private static final Logger LOGGER = LogManager.getLogger(ConverterServiceDBImpl.class.getName());
    private RateGettingUtils rateUtil = new RateGettingUtils();
    private ScheduledExecutorService scheduler;
    private RunnableUtils runnable;
    Dao dao;

    /**
     * Scheduler starts when service's object is created.
     */
    public ConverterServiceDBImpl() {
        rateUtil = new RateGettingUtils();
        scheduler = Executors.newScheduledThreadPool(1);
        dao = new DaoImpl();
        dao.createTable();
        runnable = new RunnableUtils(dao);
        scheduler.scheduleAtFixedRate(runnable, 0, 1, TimeUnit.HOURS);
    }

    @RequestMapping("/buy")
    public WSResponce buyJPY(@RequestParam(value="sum", defaultValue="0.00") String sumRUB) {
        double rate = rateUtil.getRateFromDB(getDao());
        if (rate != 0) {
            double sumR = Double.parseDouble(sumRUB);
            WSResponce responce = new WSResponce(sumR/rate, "OK");
            return responce;
        }

        WSResponce responce = new WSResponce(0.00, "ERROR");
        return responce;
    }

    @RequestMapping(method = RequestMethod.GET, path = "/sell")
    public WSResponce sellJPY(@RequestParam(value="sum", defaultValue="0.00") String sumJPY) {
        double rate = rateUtil.getRateFromDB(getDao());
        if (rate != 0) {
            double sumJ = Double.parseDouble(sumJPY);
            WSResponce responce = new WSResponce(sumJ * rate, "OK");
            return responce;
        }

        WSResponce responce = new WSResponce(0.00, "ERROR");
        return responce;
    }

    public Dao getDao() {
        return dao;
    }
}
