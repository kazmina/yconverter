package service;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import utils.RateGettingUtils;
import utils.WSResponce;

/**
 * Service implementation, than takes rate directly from cbr service
 * Created by tatiana.kazmina on 24.01.2018.
 */
@RestController
@RequestMapping("/yconverter/direct")
public class ConverterServiceDirectImpl implements ConverterService {

    private static final Logger LOGGER = LogManager.getLogger(ConverterServiceDirectImpl.class.getName());
    private RateGettingUtils rateUtil = new RateGettingUtils();

    @RequestMapping("/buy")
    public WSResponce buyJPY(@RequestParam(value="sum", defaultValue="0.00") String sumRUB) {
        double rate = rateUtil.getRateFromXML();
        if (rate != 0) {
            double sumR = Double.parseDouble(sumRUB);
            WSResponce responce = new WSResponce(sumR/rate, "OK");
            return responce;
        }

        WSResponce responce = new WSResponce(0.00, "ERROR");
        return responce;
    }

    @RequestMapping(method = RequestMethod.GET, path = "/sell")
    public WSResponce sellJPY(@RequestParam(value="sum", defaultValue="0.00") String sumJPY) {
        double rate = rateUtil.getRateFromXML();
        if (rate != 0) {
            double sumJ = Double.parseDouble(sumJPY);
            WSResponce responce = new WSResponce(sumJ * rate, "OK");
            return responce;
        }

        WSResponce responce = new WSResponce(0.00, "ERROR");
        return responce;
    }
}

