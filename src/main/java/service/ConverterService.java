package service;

import utils.WSResponce;

/**
 * Interface for service implementations
 * Created by tatiana.kazmina on 24.01.2018.
 */
public interface ConverterService {
    WSResponce buyJPY (String sumRUB);
    WSResponce sellJPY (String sumJPY);
}
