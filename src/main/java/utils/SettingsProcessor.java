package utils;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Utility class working with settings file. Contains some static methods, returning property values.
 */
public class SettingsProcessor {
    private static final Logger SETTINGS_LOGGER = LogManager.getLogger(SettingsProcessor.class.getName());
    private static String propFileName = "config.properties";
    private static Properties props = new Properties();
    private static InputStream inputStream;

    static {
        try {
            inputStream = new FileInputStream(propFileName);
            if(inputStream==null){
                SETTINGS_LOGGER.error("Can't find config.properties on classpath!");
            }
            props.load(inputStream);

        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static boolean withProxy() {
        return Boolean.parseBoolean(props.getProperty("proxy_enable"));
    }

    public static String getConnectionURL() {
        return props.getProperty("connection_URL");
    }

    public static String getConnectionPassword() {
        return props.getProperty("connection_password");
    }

    public static String getConnectionUser() {
        return props.getProperty("connection_user");
    }

    public static String getProxyHost() {
        return props.getProperty("proxy_host");
    }

    public static Integer getProxyPort() {
        return Integer.parseInt(props.getProperty("proxy_port"));
    }
}





