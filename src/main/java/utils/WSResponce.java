package utils;

/**
 * Created by tatiana.kazmina on 24.01.2018.
 */
public class WSResponce {

    private final double sum;
    private final String status;

    public WSResponce(double sum, String status) {
        this.sum = sum;
        this.status = status;
    }

    public double getSum() {
        return sum;
    }

    public String getStatus() {
        return status;
    }


}
