package utils;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by tatiana.kazmina on 17.01.2018.
 */
public class DaoImpl implements Dao {
    private static final Logger DAO_LOG = LogManager.getLogger(DaoImpl.class.getName());
    private Connection conn;
    private RateGettingUtils rateUtil = new RateGettingUtils();

    public DaoImpl() {
        JDBCConnection jdbcConnection = new JDBCConnection();
        conn = jdbcConnection.getConnection();
    }


    /**
     * Method creates new table in database
     */
    public void createTable() {

        try {
            PreparedStatement ps = conn.prepareStatement("CREATE TABLE datesAndRates(date TIMESTAMP NOT NULL, " +
                    "rate DECIMAL(5,2) NOT NULL, PRIMARY KEY (date));");

            ResultSet rs = ps.executeQuery();

        } catch (SQLException e) {
            DAO_LOG.error("Exception while table creating in database: " + e.getMessage());
        }
    }

    /**
     * Method return rate value from db from row with max date value.
     */
    public double getRateFromDB() {
        double rate = 0;
        try {
            PreparedStatement ps = conn.prepareStatement("SELECT * FROM datesAndRates WHERE date = " +
                    "(SELECT MAX(date) FROM datesAndRates)");

            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                rate = rs.getDouble("rate");
            }

        } catch (SQLException e) {
            DAO_LOG.error("Exception while sql execution: " + e.getMessage());
        }

        return rate;
    }


    /**
     * Method adds new row with date and rate to out table datesAndRates
     */
    public void addRowToDBTable() {
        double rate = 0;
        try {
            Date currentDate = new Date();
            rate = rateUtil.getRateFromXML();
            String rateStr = String.valueOf(rate);
            SimpleDateFormat dateFormat = new SimpleDateFormat("YYYY-MM-dd hh:mm:ss");
            String dateStr = dateFormat.format(currentDate);
            PreparedStatement ps = conn.prepareStatement("INSERT INTO datesAndRates VALUES('"
                    + dateStr + "','" + rateStr + "');");
            ResultSet rs = ps.executeQuery();

        } catch (SQLException e) {
            DAO_LOG.error("Exception while sql execution: " + e.getMessage());
        }
    }
}

