package utils;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import service.Application;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Properties;

/**
 * Created by tatiana.kazmina on 17.01.2018.
 */
public class JDBCConnection {
    private static final Logger JDBC_CONN_LOGGER = LogManager.getLogger(JDBCConnection.class.getName());

    public Connection getConnection() {
        Connection connection = null;
        String connectionURL = SettingsProcessor.getConnectionURL();
        String connectionPassword = SettingsProcessor.getConnectionPassword();
        String connectionUser = SettingsProcessor.getConnectionUser();


            try {
                connection = DriverManager.getConnection(connectionURL, connectionUser, connectionPassword);
            } catch (Exception e) {
                JDBC_CONN_LOGGER.error(e.getMessage());
            }
            return connection;
        }
    }

