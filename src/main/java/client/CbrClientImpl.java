package client;

import org.apache.http.HttpHost;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.DefaultProxyRoutePlanner;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.dom4j.Document;
import org.dom4j.io.SAXReader;
import org.dom4j.tree.DefaultDocument;
import service.Application;
import utils.SettingsProcessor;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

/**
 * Created by tatiana.kazmina on 12.01.2018.
 */
public class CbrClientImpl implements CbrClient {
    private static final Logger LOGGER_CLIENT = LogManager.getLogger(CbrClientImpl.class.getName());
    private static CbrClient instance;
    private static URI uri;

    public static CbrClient getInstance() {
        if (instance == null) {
            instance = new CbrClientImpl();
            try {
                uri = new URI("http://www.cbr.ru/scripts/XML_daily.asp");
            } catch (URISyntaxException e) {
                LOGGER_CLIENT.error("Wrong URI!" + e.getMessage());
            }
        }
        return instance;
    }

    public static void setUri(URI uri) {
        CbrClientImpl.uri = uri;
    }

    public static URI getUri() {
        return uri;
    }

    /**
     * Takes uri and returns HTTP-responce from external web-service for further processing and getting data
     * For usage with proxy server
     * @param uri - URI with parameter from our web-service
     * @return CloseableHttpResponce for further processing depending on required kind of directory
     * @throws ParserConfigurationException
     * @throws IOException
     */
    public CloseableHttpResponse sendCBRequest(URI uri) throws ParserConfigurationException, IOException {
        HttpHost proxy = new HttpHost(SettingsProcessor.getProxyHost(), SettingsProcessor.getProxyPort());
        DefaultProxyRoutePlanner routePlanner = new DefaultProxyRoutePlanner(proxy);
        CloseableHttpClient client = HttpClients.custom().setRoutePlanner(routePlanner).build();
        HttpGet request = new HttpGet(uri);
        LOGGER_CLIENT.info("Request sent: " + uri.toString());
        CloseableHttpResponse response = client.execute(request);
        int statusCode = response.getStatusLine().getStatusCode();
        LOGGER_CLIENT.info("Response status code: " + statusCode);
        return response;
    }

    /**
     * Analogue of CloseableHttpResponse for usage without proxy
     * Takes uri and returns HTTP-responce from external web-service for further processing and getting data
     * @param uri - URI with parameter from our web-service
     * @return CloseableHttpResponce for further processing depending on required kind of directory
     * @throws ParserConfigurationException
     * @throws IOException
     */
    public CloseableHttpResponse sendCBRequestWithoutProxy (URI uri) throws ParserConfigurationException, IOException {
        CloseableHttpClient client = HttpClients.custom().build();
        HttpGet request = new HttpGet(uri);
        LOGGER_CLIENT.info("Request sent: " + uri.toString());
        CloseableHttpResponse response = client.execute(request);
        int statusCode = response.getStatusLine().getStatusCode();
        LOGGER_CLIENT.info("Response status code: " + statusCode);
        return response;
    }

    /**
     * Returns XML representation of rates from external web-service, using URI
     * @param
     * @return org.dom4j.Document - XML Document with rates
     * @throws ParserConfigurationException
     * @throws IOException
     */
    public Document getXMLrates () throws ParserConfigurationException, IOException {
        CloseableHttpResponse response;
        if (SettingsProcessor.withProxy()) {
            response = sendCBRequest(getUri());
        } else {
            response = sendCBRequestWithoutProxy(getUri());
        }
        Document rates;
        SAXReader saxReader = new SAXReader();
        try {
            rates = saxReader.read(response.getEntity().getContent());
            LOGGER_CLIENT.info("CBClient has recieved XML Object form BFO: " + rates.asXML());
        } catch (Exception ex) {
            LOGGER_CLIENT.error("Error while read XML with SAX reader \" + ex.getMessage()");
            return null;
        }
        return rates;
    }
}
