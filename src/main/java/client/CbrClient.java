package client;

import org.apache.http.client.methods.CloseableHttpResponse;
import org.dom4j.Document;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.net.URI;

/**
 * Created by tatiana.kazmina on 12.01.2018.
 */
public interface CbrClient {
    public CloseableHttpResponse sendCBRequest(URI uri) throws ParserConfigurationException, IOException;
    public Document getXMLrates () throws ParserConfigurationException, IOException;
}
