# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* В данном репозитории лежит веб-сервис, выполненный по следующему заданию (тестовая задача на собеседование):
Создать приложение с REST API,  позволяющее производить конвертацию “рубль - японская йена” и обратно по курсу ЦБ РФ.

Приложение принимает запросы и отдает ответы в следующем формате:

1. Покупка йен за рубли

Запрос: тип запроса - GET, параметр sum - сумма в рублях, положительное десятичное число с разделителем “.”, точность дробной части - 2 знака.
http://localhost:8080/yconverter/buy?sum=123.45
Ответ: JSON-формат, sum - сумма в йенах, число, формат тот же, что в запросе, status - строка, “ОК” в случае успешного ответа, “ERROR” в случае ошибки.
{ sum: 67.89, status: “OK”}

2. Продажа йен за рубли

Запрос: тип запроса - GET, параметр sum - сумма в йенах, положительное десятичное число с разделителем “.”, точность дробной части - 2 знака.
http://localhost:8080/yconverter/sell?sum=333.44
Ответ: JSON-формат, sum - сумма в рублях, число, формат тот же, что в запросе, status - строка, “ОК” в случае успешного ответа, “ERROR” в случае ошибки.
{ sum: 555.66, status: “OK”}

Курс йены запрашивать с сайта ЦБ РФ при запуске приложения и далее с периодичностью один раз в час. Полученный курс сохранять в базе данных с указанием времени его получения.

При запросе на покупку или продажу использовать актуальный (последний по времени получения) курс, сохраненный в БД.

Адрес для получения курса ЦБ РФ: http://www.cbr.ru/scripts/XML_daily.asp
Документация и дополнительная информация: http://www.cbr.ru/scripts/Root.asp?PrtId=SXML


* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

1. Проверить и изменить параметры подключения к БД в файле config.properties в коневом каталоге.
    Значения по умолчанию:
        connection_URL=jdbc:postgresql://localhost:5432/yconverterDB
        jdbc_driver_class_name=org.postgresql.Driver
        connection_user = YConverterUser
        connection_password = QWERTY

2. Таблица в базе данных создаётся при запуске сервиса и начинает один раз в час (начиная с момента создания) добавлять
туда записи с севиса ЦБ.

3. Собрать проект командой mvn clean install. Результат - папка target, в корне которой находится файл yconverter-0.1.0.war

4. Копируем указанный выше файл в папку \webapps\ директории, где расположен Tomcat

5. Запускаем Tomcat

6. Ссылки для работы с сервисом через бд (при условии настройки томкат на порт 8080):
    http://localhost:8080/yconverter/buy?sum=123.45
    http://localhost:8080/yconverter/sell?sum=333.44
7. Ссылки для работы с сервисом без бд, запрашивая курс напрямую у ЦБ (изменить хост и порт в зависимости от параметров
сервера):
    http://localhost:8080/yconverter/direct/buy?sum=123.45
    http://localhost:8080/yconverter/direct/sell?sum=333.44

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact